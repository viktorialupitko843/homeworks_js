const form = document.querySelector(".password-form");
const passwordInput = document.getElementById("passwordInput");
const confirmPasswordInput = document.getElementById("confirmPasswordInput");
const errorMessage = document.getElementById("errorMessage");

form.addEventListener("click", function (event) {
    if (event.target.id === "showPasswordIcon1" || event.target.id === "showPasswordIcon2") {
        togglePasswordVisibility(event.target);
    } else if (event.target.id === "submitButton") {
        validatePasswords();
    }
});

function togglePasswordVisibility(icon) {
    const input = icon === showPasswordIcon1 ? passwordInput : confirmPasswordInput;
    input.type = input.type === "password" ? "text" : "password";
    icon.classList.toggle("fa-eye-slash");
}

function validatePasswords() {
    const password = passwordInput.value;
    const confirmPassword = confirmPasswordInput.value;

    if (password === confirmPassword) {
        alert("You are welcome");
        errorMessage.textContent = "";
    } else {
        errorMessage.textContent = "Потрібно ввести однакові значення";
    }
}