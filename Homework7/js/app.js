function filterBy(arr, dataType) {
    return arr.filter(function(item) {
        return typeof item !== dataType;
    });
}

const inputArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(inputArray, 'string');

console.log(filteredArray);


