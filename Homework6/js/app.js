function createNewUser() {
    const firstName = prompt('Enter your name, please');
    const lastName = prompt('Enter your last name, please');
    const birthdate = prompt('Enter your birthdate (dd.mm.yyyy), please');

    const birthdateParts = birthdate.split('.');
    const day = parseInt(birthdateParts[0], 10);
    const month = parseInt(birthdateParts[1], 10);
    const year = parseInt(birthdateParts[2], 10);

    if (isNaN(day) || isNaN(month) || isNaN(year)) {
        console.error('Invalid birthdate format.');
        return null;
    }

    const newUser = {
        firstName,
        lastName,
        birthdate,
    };

    newUser.getLogin = function () {
        const firstLetter = firstName[0].toLowerCase();
        const lastLetter = lastName.toLowerCase();
        return firstLetter + lastLetter;
    };

    newUser.getAge =  function () {
        const today = new Date();
        const birthDate = new Date(year, month - 1, day);
        let age = today.getFullYear() - birthDate.getFullYear();
        const monthDiff = today.getMonth() - birthDate.getMonth();

        if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;
    }

    newUser.getPassword =  function() {
        const firstLetter = firstName[0].toUpperCase();
        const lastNameLower = lastName.toLowerCase();
        const birthYear = year.toString();
        return firstLetter + lastNameLower + birthYear;
    }

    return newUser;
}

const user = createNewUser();
if (user) {
    console.log('User login:', user.getLogin());
    console.log('User age:', user.getAge());
    console.log('User password:', user.getPassword());
}
