function displayArrayAsList(array, parent = document.body) {
    const ul = document.createElement("ul");

    array.forEach(item => {
        const li = document.createElement("li");
        li.textContent = item;
        ul.append(li);
    });

    parent.append(ul);
}

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["1", "2", "3", "sea", "user", 23];

displayArrayAsList(array1);
displayArrayAsList(array2, document.getElementById("parent"));


