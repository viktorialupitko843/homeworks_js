const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = '#ff0000';
});

const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

const child = optionsList.childNodes;
child.forEach(node => {
    console.log(`Назва ноди: ${node.nodeName}, Тип ноди: ${node.nodeType}`);
})

//testParagraph в html у нас це id, а не клас як зазначено в умові
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');
const elements = mainHeader.querySelectorAll('*');
elements.forEach(element => {
    element.classList.add('nav-item');
});

const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(title => {
    title.classList.remove('section-title');
});
