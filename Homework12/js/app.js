
document.addEventListener('keydown', function(event) {
    const keyPressed = event.key.toUpperCase();
    const buttons = document.querySelectorAll('.btn');

    buttons.forEach(function(button) {
        const buttonText = button.textContent.trim().toUpperCase();

        if (buttonText === keyPressed) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    });
});




