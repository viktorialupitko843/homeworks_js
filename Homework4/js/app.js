do {
    let num1 = +prompt('Enter first number');
    let num2 = +prompt('Enter second number');

    while (Number.isNaN(num1 && num2)){
        num1 = +prompt('Enter first number again, please', [num1]);
        num2 = +prompt('Enter second number again, please', [num2]);
    }

    let operation = prompt('Enter math operation: +, -, *, /, **')

    while (operation !== '+' && operation !=='-' && operation !== '*' && operation !== '/' && operation!=='**'){
        operation = prompt('Enter math operation again: +, -, *, /, **', [operation]);
    }

    function result(){
        switch (operation){
            case '+':{
                return num1 + num2;
            }
            case '-':{
                return num1 - num2;
            }
            case '*':{
                return num1 * num2;
            }
            case '/':{
                return num1 / num2;
            }
            case '**':{
                return num1 ** num2;
            }
        }
    }
    console.log(`Над числами ${num1} і ${num2} була проведена операція ${operation}. Результат: ${result()}`)
}while (confirm('You want to repeat?'))